<!DOCTYPE html>
<html>
    <head>
        <title>Lista</title>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link href="css/list.css" type="text/css" rel="stylesheet">
    </head>
    <body>
        <table id="listform">
            <tr>
                <th>Namn</th>
                <th>Epost</th>
                <th>Företag</th>
                <th>Allergier</th>
            </tr>

            <?php
                require('include/connect.php');
                $sql = "SELECT * FROM `seenthis_invitation`";
                $stmt = $conn->prepare($sql);
                $stmt->execute();
                $results = $stmt->fetchAll(PDO::FETCH_ASSOC);
                // foreach

                foreach($results as $invitee) {
                    print('<tr>');
                    printf('<td>%s</td>', $invitee['name']);
                    printf('<td>%s</td>', $invitee['email']);
                    printf('<td>%s</td>', $invitee['company']);
                    printf('<td>%s</td>', $invitee['allergy']);
                    print('</tr>');
                }


            ?>

        </table>
    </body>
</html>