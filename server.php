<?php

    require('aws/aws-autoloader.php');

    use Aws\Ses\SesClient;

    $client = SesClient::factory(array(
        'version' => 'latest',
        'credentials' => array(
            'key' => 'AKIAJUX5LYWO6DFMS4XA',
            'secret' => '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'),
        'region' => 'eu-west-1',
        'http' => array(
            'verify' => 'cacert.pem'
        )
    ));

    if(isset($_POST['party']['submit'])) {

        $errors = array();

        try {
            $sql = "CREATE TABLE IF NOT EXISTS `seenthis_invitation` (
                id INT(6) NOT NULL AUTO_INCREMENT PRIMARY KEY,
                name VARCHAR(255) NOT NULL,
                email VARCHAR(255) NOT NULL,
                company VARCHAR(255) NOT NULL,
                allergy VARCHAR(255) NULL,
                accdate DATE NOT NULL)";

            $conn->exec($sql);
        } catch(PDOexception $e) {
            echo $e->getMessage();
        }

        $name = $_POST['party']['name'];
        $email = $_POST['party']['email'];
        $company = $_POST['party']['company'];
        $allergy = $_POST['party']['allergy'];
        $date = date("Y-m-d");

        if(empty($name) || empty($email) || empty($company)) {
            $errors[] = "Fält är obligatoriskt";
        } else {

            $sql = "INSERT INTO `seenthis_invitation` (`name`, `email`, `company`, `allergy`, `accdate`)
                    VALUES (:name, :email, :company, :allergy, :tdate)";
            
            $stmt = $conn->prepare($sql);
            $stmt->bindParam(':name', $name);
            $stmt->bindParam(':email', $email);
            $stmt->bindParam(':company', $company);
            $stmt->bindParam(':allergy', $allergy);
            $stmt->bindParam(':tdate', $date);

            $stmt->execute();

            /* SEND MAIL FROM AWS */

            $result = $client->sendEmail(array(
                // Source is required
                'Source' => 'Seenthis <contact@seenthis.se>',
                // Destination is required
                'Destination' => array(
                    'ToAddresses' => array($email),
                ),
                // Message is required
                'Message' => array(
                    // Subject is required
                    'Subject' => array(
                        // Data is required
                        'Data' => 'Tack för din anmälan!',
                        'Charset' => 'utf-8',
                    ),
                    // Body is required
                    'Body' => array(
                        'Text' => array(
                            // Data is required
                            'Data' => 'Tack för din anmälan till Seenthis heta kväll den 26e oktober!Du är varmt välkommen förbi Fatbursgatan 1 från klockan 17. Om du vill kika på inbjudan igen så hittar du den enklast här.
                            
                            Om du skulle få förhinder så avanmäler du dig genom att klicka på följande länk Avanmälan.
                            
                            Med vänliga hälsningar,
                            Seenthis',
                            'Charset' => 'utf-8',
                        ),
                        'Html' => array(
                            'Data' => '<html><head><body><p>Tack för din anmälan till Seenthis sensommarkväll den 26e oktober!<br>Du är varmt välkommen förbi <a href="https://www.google.se/maps/place/Fatbursgatan+1,+118+28+Stockholm/@59.3146533,18.0650699,17z/data=!3m1!4b1!4m5!3m4!1s0x465f77eff64476d1:0xe3c9c7c0b1177c4!8m2!3d59.3146514!4d18.0666175">Fatbursgatan 1 (karta)</a> från klockan 17. Om du vill kika på inbjudan igen så hittar du den enklast <a href="http://seenthis.co/fest">här</a>.<br><br>Om du skulle få förhinder så avanmäler du dig genom att klicka på följande länk <a href="mailto:fest@seenthis.co?subject=Avanmälan 26/10">Avanmälan.</a><br><br>Med vänliga hälsningar,<br>Seenthis</p></body></head></html>',
                        ),
                    ),
                ),
                'ReturnPath' => 'contact@seenthis.se',
            ));
        }          
    }

?>