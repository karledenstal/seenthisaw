<!DOCTYPE html>
<html>
    <head>
        <title>Seenthis Fest 26/10</title>
        <link href="css/seenthis.css" type="text/css" rel="stylesheet">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <script src="js/popup.js"></script>
    </head>
    <body>

    <?php

        require('include/connect.php');
        require('server.php');
        // nåt smut kmr hända här!
        // var_dump($_SERVER);

        if(isset($_POST['party']['submit'])) {
            include('include/popper.html');
        }
    ?>

        <div id="wrapper">
            <div class="invite left">
                <img src="img/SeenthisAW2left.png">
            </div>
            <div class="invite right">
                <img src="img/SeenthisAW2right.png">
                <div class="oct"><img src="img/26octgififnal.gif"></div>
                <p class="text">
                    Visst kan det kännas grått och trist såhär års?<br><br>
                    
                    Det ska vi på Seenthis ändra på!<br><br>
                    
                    Torsdagen 26 Oktober bjuder vi därför in er till en het kväll på vårt kontor.<br><br>
                    Det kommer finnas både mat, dryck och liveband på plats.<br>
                    En kväll du inte vill missa!<br><br>
                    
                    Tid: Från kl 17 och framåt<br>
                    Plats: Fatbursgatan 1<br><br>
                    
                    <a href="#" class="osa-link" data-modal-target="osa-pop">Osa genom att klicka här</a><br><br>
                    
                    //<br><br>
                    
                    Osa senast 19/10
                </p>
            </div>
            <div id="osa-pop" class="content">
                <div class="form">
                    Vad kul att du vill komma!<br>
                    OSA genom att skriva in dig nedan och så får du ett bekräftelsemail.<br><br>

                    <form id="inviteform" method="post" action="<?php echo htmlspecialchars($_SERVER['PHP_SELF']); ?>">
                        <label for="name">Namn</label>
                        <input required="required" id="name" type="text" name="party[name]" placeholder="Ditt namn">
                        <label for="email">Email</label>
                        <input required="required" id="email" type="email" name="party[email]" placeholder="dinemail@mail.com">
                        <label for="company">Företag</label>
                        <input id="company" type="text" name="party[company]" placeholder="Företag">
                        <label for="allergy">Har du allergier eller särskilda matpreferenser?<br>
                        Vänligen ange nedan!</label>
                        <textarea name="party[allergy]" id="allergy" placeholder="Allergier"></textarea>
                        <button type="submit" id="submitbtn" name="party[submit]">Jag kommer!</button>
                    </form>
                </div>
            </div>
        </div>

    </body>
</html>