$(document).ready(function() {

    var fadebg = $(document.createElement("div")).hide().attr("id", "fadebg"),
    modal = $(document.createElement("div")).hide().attr("id", "formbg");

    $(document.body).append(fadebg, modal);
    $(document).on('click', '.osa-link', function(e) {
        $('#fadebg').fadeIn('fast');
        var contents = $("#" + $(this).data("modal-target")).html();
        $('#formbg').html(contents);
        $('#formbg').fadeIn('fast');
        e.preventDefault();
    });

    $(document).on('click', '#fadebg', function() {
        $("#fadebg").fadeOut("fast");
        $("#formbg").fadeOut("fast");
    });

    $(document).keydown(function(e) {
        if(e.keyCode == 27) {
            $("#fadebg").fadeOut("fast");
            $("#formbg").fadeOut("fast");
        }
    });

    $('#close-ref').click(function(e) {
        $('#box-ref').hide();
        e.preventDefault();
    });

    $('#submitbtn').click(function(e){
        console.log($('#name').val);
        console.log($('#email').val);
        e.preventDefault();
    });
});